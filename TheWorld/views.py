from django.shortcuts import get_object_or_404, render, redirect
from django.utils import timezone
from django.views import generic
from django.views.generic import CreateView

from TheWorld.forms import CommentForm, PostForm
from TheWorld.models import Post


class PostList(generic.ListView):
    queryset = Post.objects.filter().order_by('published_date')
    template_name = 'TheWorld/post_list.html'


def post_detail(request, pk):
    template_name = 'TheWorld/post_detail.html'
    post = get_object_or_404(Post, pk=pk)
    comments = post.comments.filter(active=True)
    new_comment = None
    # Comment posted
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            # Create Comment object but don't save to database yet
            new_comment = comment_form.save(commit=False)
            # Assign the current post to the comment
            new_comment.post = post
            # Save the comment to the database
            new_comment.save()
    else:
        comment_form = CommentForm()

    return render(request, template_name, {'post': post,
                                           'comments': comments,
                                           'new_comment': new_comment,
                                           'comment_form': comment_form})


def AddPost(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'TheWorld/add_post.html', {'form': form})
